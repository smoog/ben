import React from 'react';
import Navbar from './components/Navbar';
import './App.css';
import Home from './components/pages/Home';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Acceuil from './components/pages/Services';
import Products from './components/pages/Products';
import Qui from './components/pages/Qui';

function App() {
  return (

      <Router>
        <Navbar />
        <Switch>
          <Route path='/' exact component={(Home)} />
          <Route path='/acceuil' component={(Acceuil)} />
          <Route path='/qui' component={(Qui)} />
          <Route path='/products' component={(Products)} />

        </Switch>
      </Router>

  );
}

export default App;