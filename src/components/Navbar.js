import React,{useState,useEffect} from'react';// use Effect c'est un Hook et c'est pour disparetre le bouton de sign-up quand on fait rafrichir sur la page
import {Link} from 'react-router-dom';
import './Navbar.css'; 
import { Button } from './Button';
import {connect} from "react-redux";


const Navbar =(props)=> { //c'est pour the state
    const [click,setClick] = useState(false);
    const [button,setButton] = useState(true)

    const handleClick =() => setClick(!click);
    const closeMobileMenu =() => setClick(false);
    
    //c'est pour ajuster la taille de l'ecrant 'tlfn-pc..''show'
    const showButton =() =>{
        if (window.innerWidth <= 960){
            setButton(false);
        } else {
            setButton(true);
        }

    };

    useEffect(() => {//the hook 
      showButton();  
      //console.log("button", button)
      //console.log("click", click)
    },[]);
    
    window.addEventListener('resize',showButton);
    
    
    return(//on click pour suprimé la navabar "linge 19"
    //j'ai utilisé "onClick={closeMobileMenu} " pour que je puise quliquer sur le logo pour retourner a l'acceil ligne 35
    
          <nav className='navbar'>
          
            <div className='navbar-container'>
              <Link to='/' className='navbar-logo' onClick={closeMobileMenu}>
                Phuvanios 
                <i class='fab fa-typo3' />
              </Link>
              <div className='menu-icon' onClick={handleClick}>
                <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
              </div>
              <ul className={click ? 'nav-menu active' : 'nav-menu'}>
                <li className='nav-item'>
                  <Link to='/' className='nav-links' onClick={closeMobileMenu}>
                    Acceuil
                  </Link>
                </li>
                {<li className='nav-item'>
                  <Link
                    to='/services'
                    className='nav-links'
                    onClick={closeMobileMenu}
                  >
                    Qui sommes-nous?
                  </Link>
                </li>}
                <li className='nav-item'>
                  <Link
                    to='/products'
                    className='nav-links'
                    onClick={closeMobileMenu}
                  >
                    Produits
                  </Link>
                </li>

                <li className='nav-item'>
                <Link
                  to='/products'
                  className='nav-links'
                  onClick={closeMobileMenu}
                >
                  Conditions
                </Link>
              </li>

                

                 
              
              
                {!props.user.isLogged && <li className='nav-item'>
                  {button && <Button buttonStyle='btn--outline' url="/contact">Contact</Button>}
                  {button === false && <Link
                    to='/Contact'
                    className='nav-links-mobile'
                    onClick={closeMobileMenu}
                  >
                    Contact
                  </Link>}
                </li>}
                
                
              
              </ul>
              
            </div>
          </nav>

      );
    }
  

    const mapDispatchToProps = {

    }
    
    const mapStateToProps = (store)=>{
      return {
        user: store.user
      }
    }
  
  
    export default connect(mapStateToProps, mapDispatchToProps)(Navbar);

    //                ligne 85  {button && <Button buttonStyle='btn--outline' url="/sign-up">SIGN UP</Button>}
    //                  {button === false && <Link

    //        ligne 97      button && <Button buttonStyle='btn--outline' url="/login">Login</Button>}
     //             {button === false && <Link