import React from 'react';
import '../App.css';
import './HeroSection.css';

function HeroSection() {
  return (
    <div className='hero-container'>
      <video src='' autoPlay loop muted />
      <h1>Bienvenue sur notre site</h1>
      <p>qu'attendez-vous ?</p>
      <div className='hero-btns'>
      
       
      </div>
    </div>
  );
}

export default HeroSection;