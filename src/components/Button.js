import React from 'react';
import './Button.css';
import {Link} from 'react-router-dom';

const STYLES = ['btn--primary', 'btn--outline'];// pour faire les buttons 'get started' and 'watch trailer'

//const SIZES = ['btn--medium', 'btn--large'];//css pour les deux buttons outline et primary

export const Button = ({
    children,
    type, 
    onClick,
    buttonStyle,
    buttonSize,
    url
}) =>{
    const checkButtonStyle =STYLES.includes(buttonStyle)
     ? buttonStyle 
     : STYLES[0];

     //const checkButtonSize = SIZES.includes(buttonSize) ? buttonSize : SIZES[0]//pour choisir en defaut la premiere si il trouve pas une commande

     return(
         <Link to={url} className='btn-mobile'>
             <button
             className={checkButtonStyle} /*{checkButtonSize}*/
             onClick={onClick}
             type={type}
             >
                 {children}
             </button>
         </Link>

     );
};



