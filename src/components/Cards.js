import React from 'react'
import CardItem from './Carditem'
import "./Cards.css";

function Cards(){
    return(
        <div className='cards'>
            <h1>N'oubliez pas de consulter nos produits!</h1>
            <div className="cards__container">
                <div className="cards__wrapper">
                    <ul className="cards__items">
                        <CardItem
                        src="../images/general2.jpg" 
                        text="Des Huiles Essentielles et Végétales, pures et naturelles Provenant des différentes parties du Monde Santé, Beauté, Bien-être, Soins, Relaxation, Massage... Des plantes sauvages et ancestrales riches en énergie Depuis 2008 Madatrano vous propose des huiles naturelles authentiques et rares certifiées au juste prix Bio ou Conventionnelle - Qualité Certifiée ! Découvrez la richesse des arômes de Madagascar grâce à notre sourcing local et traditionnel Pour Aromathérapie, Massages et Diffuseurs."
                        label='Qui sommes-nous?'//titre de la photo
                        path='/services'
                        />
                        <CardItem
                        src="images/first.jpg" 
                        text="Huiles Essentielles 100% Pures et Naturelles Gamme importante, Flacons et Roll-on multiples formats. Pour: Soigner, Masser, Relaxer, Purifier... De Madagascar: Puissantes, Rares, Endémiques. Du Monde: Indispensables, Connues et Inédites."
                        label='Les Huiles Essentielles 100% Pures et Naturelles'//titre de la photo
                        path='/Product'
                        />
                    </ul>
                    <ul className="cards__items">
                        <CardItem
                        src="images/second.jpg" 
                        text="Huiles Végétales au Litre 100% pures et Naturelles Sans Ogm ni phtalates, Certificats d'analyses, Msds A partir de 1 Litre.
                        "
                        label='Les Huiles Végétales au Litre'//titre de la photo
                        path='/services'
                        />
                        <CardItem
                        src="images/kilos.jpg" 
                        text="Huiles Essentielles au Kilo 100% Pures et Naturelles, Chémotypées Sans Ogm ni Phtalates, Certificats d'analyses, Msds. Flacon Aluminium et bouchon sécurité."
                        label='Les Huiles Essentielles au Kilo'//titre de la photo
                        path='/services'
                        />
                         <CardItem
                        src="images/pdts.jpg" 
                        text="Découvrez nos produits!!"
                        label='Découvrez nos produits!!'//titre de la photo
                        path='/services'
                        />
                    </ul>
                </div>

            </div>
        </div>
    )
}

export default Cards; 